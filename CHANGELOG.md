# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.13.0

- minor: Internal maintenance: Update boto3 to 1.35.*.
- patch: Internal maintenance: Update pipes versions in pipelines config file.
- patch: Internal maintenance: Update tests.

## 1.12.2

- patch: Internal maintenance: Bump release pipe version. Add multi-platform build: linux/arm64 and linux/amd64.

## 1.12.1

- patch: Internal maintenance: Update tests.

## 1.12.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 1.11.0

- minor: Update boto3 to 1.33.*.
- patch: Internal maintenance: refactor logging.
- patch: Internal maintenance: update pipes versions inside pipelines config file.

## 1.10.1

- patch: Add extra logs about a revision - version number of the registered task definition.

## 1.10.0

- minor: Implemented support for AWS STS (using assume-role functionality).
- patch: Fix broken test.

## 1.9.1

- patch: Internal maintenance: update pipes versions inside pipelines config file.
- patch: Remove obsolete IMAGE_NAME variable from the pipe logic. It should be defined inside task-definition.json file.

## 1.9.0

- minor: Add WAIT_INTERVAL and WAIT_MAX_ATTEMPTS env vars for more control over the waiter config.
- minor: Update boto3 version to 1.28.
- patch: Internal maintenance: update package and pipes versions in requirements and pipelines config file.

## 1.8.0

- minor: Internal maintenance: Update bitbucket-pipes-toolkit to fix vulnerability with certify CVE-2023-37920.

## 1.7.0

- minor: Internal maintenance: Bump version of the base Docker image to python:3.10-slim.
- patch: Internal maintenance: Bump version of boto3 and other dependencies.
- patch: Internal maintenance: Bump version of the bitbucket-pipe-release.
- patch: Internal maintenance: Update the community link.

## 1.6.2

- patch: Internal maintenance: Update package versions in requirements and test requirements.
- patch: Internal maintenance: Update pipes versions of aws-cloudformation-deploy and bitbucket-pipe-release.

## 1.6.1

- patch: Internal maintenance: Add required tags for test infra resources.

## 1.6.0

- minor: Bump pipes-toolkit -> 2.2.0.

## 1.5.0

- minor: Support AWS OIDC authentication. Environment variables AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY are not required anymore.

## 1.4.0

- minor: Make TASK_DEFINITION variable optional. It could be useful when FORCE_NEW_DEPLOYMENT variable used. Use this option if you need to trigger a new deployment with no service definition changes: if your updated Docker image uses the same tag as what is in the existing task definition for your service (for example, my_image:latest).

## 1.3.0

- minor: Add support for the FORCE_NEW_DEPLOYMENT variable. Use this option if you need to trigger a new deployment with no service definition changes: if your updated Docker image uses the same tag as what is in the existing task definition for your service (for example, my_image:latest).

## 1.2.1

- patch: Internal maintenance: bump bitbucket-pipe-release.

## 1.2.0

- minor: Internal maintenance: bump bitbucket-pipes-toolkit version.

## 1.1.4

- patch: Internal maintenance: change pipe metadata according to new structure

## 1.1.3

- patch: Internal maintenance: Update pipe's depencies.

## 1.1.2

- patch: Internal maintenance: Add gitignore secrets.

## 1.1.1

- patch: Internal maintenance: Upgrade dependency version bitbucket-pipes-toolkit.
- patch: Update the Readme with a new Atlassian Community link.
- patch: Update the Readme with details about default variables.

## 1.1.0

- minor: Add default values for AWS variables.

## 1.0.7

- patch: Internal maintenance: Add hadolint linter for Dockerfile

## 1.0.6

- patch: Add warning message about new version of the pipe available.

## 1.0.5

- patch: Internal maintenance: Add auto infrastructure for tests.

## 1.0.4

- patch: Fix readme example.

## 1.0.3

- patch: Internal release

## 1.0.2

- patch: Added code style checks

## 1.0.1

- patch: Fixed log's messages

## 1.0.0

- major: Parameter names were simplified

## 0.3.0

- minor: Fix the bug when one of the parameters was validated as required while it was actually optional

## 0.2.2

- patch: Fixed the bug with missing pipe.yml

## 0.2.1

- patch: Fixed the docker image

## 0.2.0

- minor: Changed pipe to use the full task definition json instead of only using the container definitions

## 0.1.0

- minor: Initial release
