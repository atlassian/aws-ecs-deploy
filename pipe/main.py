import configparser
import json
from pprint import pformat
import os
import stat
import time

import yaml
import boto3
from bitbucket_pipes_toolkit import Pipe
from botocore.exceptions import ClientError, ParamValidationError, WaiterError

WAIT_INTERVAL_DEFAULT = 15  # sec
WAIT_MAX_ATTEMPTS_DEFAULT = 40

schema = {
    "AWS_ACCESS_KEY_ID": {
        "type": "string",
        "required": True
    },
    "AWS_SECRET_ACCESS_KEY": {
        "type": "string",
        "required": True
    },
    "AWS_ROLE_ARN": {
        "type": "string",
        "required": False
    },
    "AWS_ROLE_SESSION_NAME": {
        "type": "string",
        "required": False
    },
    "AWS_DEFAULT_REGION": {
        "type": "string",
        "required": True
    },
    "CLUSTER_NAME": {
        "type": "string",
        "required": True
    },
    "SERVICE_NAME": {
        "type": "string",
        "required": True
    },
    "TASK_DEFINITION": {
        "type": "string",
        "required": False
    },
    "FORCE_NEW_DEPLOYMENT": {
        "type": "boolean",
        "default": False
    },
    "WAIT": {
        "type": "boolean",
        "default": False
    },
    'WAIT_INTERVAL': {
        'type': 'integer',
        'required': False,
        'default': WAIT_INTERVAL_DEFAULT
    },
    "WAIT_MAX_ATTEMPTS": {
        'type': 'integer',
        'required': False,
        "default": WAIT_MAX_ATTEMPTS_DEFAULT
    },
    "DEBUG": {
        "type": "boolean",
        "default": False
    }
}


class ECSDeploy(Pipe):
    OIDC_AUTH = 'OIDC_AUTH'
    DEFAULT_AUTH = 'DEFAULT_AUTH'

    def __init__(self, pipe_metadata=None, schema=None, env=None, client=None, check_for_newer_version=False):
        self.auth_method = self.discover_auth_method()
        self.role_arn_check = self.resolve_role_arn()
        super().__init__(pipe_metadata=pipe_metadata, schema=schema, env=env,
                         check_for_newer_version=check_for_newer_version)
        self.client = client
        self.action = None

        self.boto3_client_name = 'ecs'
        self.region = self.get_variable('AWS_DEFAULT_REGION')
        self.definition = self.get_variable('TASK_DEFINITION')
        self.cluster_name = self.get_variable('CLUSTER_NAME')
        self.service_name = self.get_variable('SERVICE_NAME')
        self.force_new_deployment = self.get_variable('FORCE_NEW_DEPLOYMENT')

    def discover_auth_method(self):
        """Discover user intentions: authenticate to AWS through OIDC or default aws access keys"""
        oidc_role = os.getenv('AWS_OIDC_ROLE_ARN')
        web_identity_token = os.getenv('BITBUCKET_STEP_OIDC_TOKEN')
        if oidc_role:
            if web_identity_token:
                schema['BITBUCKET_STEP_OIDC_TOKEN'] = {'required': True}
                schema['AWS_OIDC_ROLE_ARN'] = {'required': True}

                schema['AWS_ACCESS_KEY_ID']['required'] = False
                schema['AWS_SECRET_ACCESS_KEY']['required'] = False

                os.environ.pop('AWS_ACCESS_KEY_ID', None)
                os.environ.pop('AWS_SECRET_ACCESS_KEY', None)

                return self.OIDC_AUTH

            return self.DEFAULT_AUTH

        return self.DEFAULT_AUTH

    @staticmethod
    def resolve_role_arn():
        if os.getenv('AWS_ROLE_ARN'):
            schema['AWS_ROLE_ARN']['required'] = True
            schema['AWS_ROLE_SESSION_NAME']['required'] = True

            return True

        return False

    def auth(self):
        """Authenticate via chosen method"""
        if self.auth_method == self.OIDC_AUTH:
            self.log_info('Authenticating with a OpenID Connect (OIDC) Web Identity Provider.')
            random_number = str(time.time_ns())
            aws_config_directory = os.path.join(os.environ["HOME"], '.aws')
            oidc_token_directory = os.path.join(aws_config_directory, '.aws-oidc')

            os.makedirs(aws_config_directory, exist_ok=True)
            os.makedirs(oidc_token_directory, exist_ok=True)

            web_identity_token_path = os.path.join(f'{aws_config_directory}/.aws-oidc', f'oidc_token_{random_number}')
            with open(web_identity_token_path, 'w') as f:
                f.write(self.get_variable('BITBUCKET_STEP_OIDC_TOKEN'))
            os.chmod(web_identity_token_path, mode=stat.S_IRUSR)

            self.log_debug('Web identity token file is created')

            aws_configfile_path = os.path.join(aws_config_directory, 'config')
            with open(aws_configfile_path, 'w') as configfile:
                config = configparser.ConfigParser()
                config['default'] = {
                    'role_arn': self.get_variable('AWS_OIDC_ROLE_ARN'),
                    'web_identity_token_file': web_identity_token_path
                }
                config.write(configfile)

            self.log_debug('Configured settings for authentication with assume web identity role')
        elif self.auth_method == self.DEFAULT_AUTH:
            if os.getenv('AWS_OIDC_ROLE_ARN'):
                self.log_warning('Parameter "oidc: true" in the step configuration is required for OIDC authentication')

            self.log_info('Using default authentication with AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY.')
        else:
            self.fail("Auth method not supported.")

    def get_client(self):
        try:
            if self.role_arn_check:
                role_arn = self.get_variable('AWS_ROLE_ARN')
                role_session_name = self.get_variable('AWS_ROLE_SESSION_NAME')
                self.log_info('Using STS assume role with AWS_ROLE_ARN')

                session = self.role_arn_to_session(role_arn, role_session_name)

                return session.client(self.boto3_client_name, region_name=self.region)

            return boto3.client(self.boto3_client_name, region_name=self.region)
        except ClientError as err:
            self.fail(f"Failed to create boto3 client.\n Error: {str(err)}")

    @staticmethod
    def role_arn_to_session(role_arn, role_session_name):
        client = boto3.client('sts')
        response = client.assume_role(
            RoleArn=role_arn,
            RoleSessionName=role_session_name
        )

        return boto3.Session(
            aws_access_key_id=response['Credentials']['AccessKeyId'],
            aws_secret_access_key=response['Credentials']['SecretAccessKey'],
            aws_session_token=response['Credentials']['SessionToken'])

    def _handle_update_service_error(self, error):
        error_code = error.response['Error']['Code']
        if error_code == 'ClusterNotFoundException':
            msg = 'ECS cluster not found. Check your CLUSTER_NAME.'
        elif error_code == 'ServiceNotFoundException':
            msg = 'ECS service not found. Check your SERVICE_NAME'
        else:
            msg = "Failed to update the stack.\n" + str(error)
        self.fail(msg)

    def update_task_definition(self, task_definition_file):

        self.log_info('Updating the task definition...')

        client = self.get_client()
        try:
            with open(task_definition_file) as d_file:
                task_definition = json.load(d_file)
        except json.decoder.JSONDecodeError:
            self.fail('Failed to parse the task definition file: invalid JSON provided.')
        except FileNotFoundError:
            self.fail(f'Not able to find {task_definition_file} in your repository.')

        self.log_info(f'Using task definition: \n{pformat(task_definition)}')

        try:
            response = client.register_task_definition(**task_definition)

            self.log_info(f"Revision number: {response['taskDefinition']['revision']}")

            return response['taskDefinition']['taskDefinitionArn']
        except ClientError as err:
            self.fail("Failed to update the stack.\n" + str(err))
        except KeyError as err:
            self.fail("Unable to retrieve taskDefinitionArn key.\n" + str(err))
        except ParamValidationError as err:
            self.fail(f"ECS task definition parameter validation error: \n {err.args[0]}")

    def list_task_definitions(self):

        self.log_info('Check for active task definition...')

        client = self.get_client()

        try:
            response = client.list_task_definitions()
            return response.get('taskDefinitionArns')
        except ClientError as err:
            self.fail("Failed to fetch list of task definitions.\n" + str(err))

    def update_service(self, cluster, service, task_definition=None, force_new_deployment=False):

        self.log_info(f'Update the {service} service.')

        client = self.get_client()

        parameters = {
            "cluster": cluster,
            "service": service,
            "taskDefinition": task_definition,
            "forceNewDeployment": force_new_deployment,
        }

        if task_definition is None:
            parameters.pop('taskDefinition')

        try:
            response = client.update_service(**parameters)
            return response
        except ClientError as err:
            self._handle_update_service_error(err)

    def run(self):
        self.auth()

        if self.definition is None:
            task_definition = None
            # try to check any active task_definitions on the service
            if not self.list_task_definitions():
                self.fail(
                    f'There are no active tasks definitions for the service {self.service_name} on the cluster {self.cluster_name}. '
                    f'Check your service on the AWS Console or provide the TASK_DEFINITION variable.'
                )
        else:
            task_definition = self.update_task_definition(self.definition)

        response = self.update_service(
            self.cluster_name, self.service_name, task_definition, self.force_new_deployment)

        self.log_debug(response)

        if self.get_variable('WAIT'):
            self.log_info('Waiting for service to become Stable...')

            wait_interval = self.get_variable('WAIT_INTERVAL')
            wait_max_attempts = self.get_variable('WAIT_MAX_ATTEMPTS')

            client = self.get_client()
            waiter = client.get_waiter('services_stable')
            try:
                waiter.wait(
                    cluster=self.cluster_name,
                    services=[self.service_name],
                    WaiterConfig={
                        'Delay': wait_interval,
                        'MaxAttempts': wait_max_attempts
                    }
                )
            except WaiterError as e:
                self.fail(f'Error waiting for service to become stable: {e}')
            self.log_info(f'Service {self.service_name} has become stable')

        self.success(f'Successfully updated the {self.service_name} service. You can check you service here: \n'
                     f'https://console.aws.amazon.com/ecs/home?region={self.region}#/clusters/{self.cluster_name}/services/{self.service_name}/details')


if __name__ == '__main__':
    with open('/pipe.yml', 'r') as metadata_file:
        metadata = yaml.safe_load(metadata_file.read())
    pipe = ECSDeploy(pipe_metadata=metadata, schema=schema, check_for_newer_version=True)
    pipe.run()
