import configparser
from copy import copy
import json
import logging
import os
import shutil
import sys
from unittest import TestCase

import yaml
import pytest
import botocore.session
from botocore.stub import Stubber, ANY
from botocore.waiter import Waiter

from pipe.main import ECSDeploy, schema
from test.test import task_definition


class SuccessTest(TestCase):
    @pytest.fixture(autouse=True)
    def inject_fixtures(self, caplog, capsys, mocker):
        self.caplog = caplog
        self.capsys = capsys
        self.mocker = mocker

    @classmethod
    def setUpClass(cls):
        # update task_definition
        td = json.loads(task_definition)
        td['family'] = 'aaa'
        td['containerDefinitions'][0]['name'] = 'aaa'

        with open('test/taskDefinition.json', 'w') as outfile:
            json.dump(td, outfile)

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
        os.remove('test/taskDefinition.json')

    def test_aws_ecs_deploy(self):
        self.mocker.patch.dict(
            os.environ,
            {
                'CLUSTER_NAME': 'fake-cluster',
                'SERVICE_NAME': 'fake-service',
                'TASK_DEFINITION': 'test/taskDefinition.json',
                'WAIT': 'True',
                'WAIT_INTERVAL': '20',
                'WAIT_MAX_ATTEMPTS': '30',
                'AWS_ACCESS_KEY_ID': 'akiafake',
                'AWS_SECRET_ACCESS_KEY': 'fakesecretkey',
                'AWS_DEFAULT_REGION': 'test-region',
                'BITBUCKET_COMMIT': 'abecdf',
                'BITBUCKET_BUILD_NUMBER': '1',
                'BITBUCKET_REPO_FULL_NAME': 'fake/fake',
                'AWS_OIDC_ROLE_ARN': ''
            }
        )

        mock_ecs = self.mocker.patch.object(ECSDeploy, 'get_client')
        ecs = botocore.session.get_session().create_client('ecs')
        mock_ecs.return_value = ecs

        mock_wait = self.mocker.patch.object(Waiter, 'wait')

        with open('test/taskDefinition.json', 'r') as td:
            task_definition = json.load(td)

        stubber = Stubber(ecs)
        stubber.add_response(
            'register_task_definition',
            {'taskDefinition': {'taskDefinitionArn': 'aaa', 'revision': 111}},
            expected_params=task_definition
        )
        stubber.add_response(
            'update_service',
            {
                'service': {
                    'serviceName': 'fake-service'
                }
            },
            expected_params={
                'cluster': 'fake-cluster',
                'service': 'fake-service',
                'taskDefinition': ANY,
                'forceNewDeployment': False,
            }
        )
        stubber.add_response(
            'describe_services',
            {
                'services': [
                    {
                        'serviceName': 'fake-service',
                        'deployments': [
                            {'taskDefinition': 'aaa'}
                        ]
                    }
                ],
                'failures': []
            },
            expected_params={
                'cluster': ANY,
                'services': ANY
            }
        )

        stubber.activate()

        with open(os.path.join(os.getcwd(), 'pipe.yml')) as f:
            metadata = yaml.safe_load(f.read())

        ecs_deploy_pipe = ECSDeploy(
            schema=schema,
            pipe_metadata=metadata,
            check_for_newer_version=True
        )
        ecs_deploy_pipe.run()

        out = self.capsys.readouterr().out

        mock_wait.assert_called_with(
            ANY,
            cluster='fake-cluster',
            services=['fake-service'],
            WaiterConfig={'Delay': 20, 'MaxAttempts': 30})

        self.assertRegex(
            out,
            f"✔ Successfully updated the {os.getenv('SERVICE_NAME')} service"
        )


class NegativeTest(TestCase):
    @pytest.fixture(autouse=True)
    def inject_fixtures(self, caplog, capsys, mocker):
        self.caplog = caplog
        self.capsys = capsys
        self.mocker = mocker

    def test_aws_ecs_deploy(self):
        self.mocker.patch.object(ECSDeploy, 'update_task_definition', return_value='aaa')
        self.mocker.patch.object(ECSDeploy, 'list_task_definitions', return_value=[])
        self.mocker.patch.object(ECSDeploy, 'update_service', return_value='response')
        self.mocker.patch.dict(
            os.environ,
            {
                'CLUSTER_NAME': 'fake-cluster',
                'SERVICE_NAME': 'fake-service',
                'WAIT': 'True',
                'AWS_ACCESS_KEY_ID': 'akiafake',
                'AWS_SECRET_ACCESS_KEY': 'fakesecretkey',
                'AWS_DEFAULT_REGION': 'test-region',
                'BITBUCKET_COMMIT': 'abecdf',
                'BITBUCKET_BUILD_NUMBER': '1',
                'BITBUCKET_REPO_FULL_NAME': 'fake/fake',
                'AWS_OIDC_ROLE_ARN': ''
            }
        )

        with open(os.path.join(os.getcwd(), 'pipe.yml')) as f:
            metadata = yaml.safe_load(f.read())

        ecs_deploy_pipe = ECSDeploy(
            schema=schema,
            pipe_metadata=metadata,
            check_for_newer_version=True
        )
        with self.assertRaises(SystemExit) as exc_context:
            ecs_deploy_pipe.run()
            self.assertEqual(exc_context.exception.code, SystemExit)

        out = self.capsys.readouterr().out

        self.assertRegex(
            out,
            f"✖ There are no active tasks definitions for the service {os.getenv('SERVICE_NAME')} on the cluster {os.getenv('CLUSTER_NAME')}"
        )


class ECSDeployAuthTestCase(TestCase):
    @pytest.fixture(autouse=True)
    def inject_fixtures(self, caplog, capsys, mocker):
        self.caplog = caplog
        self.capsys = capsys
        self.mocker = mocker

    @classmethod
    def setUpClass(cls):
        cls.sys_path = copy(sys.path)
        sys.path.insert(0, os.getcwd())

    @classmethod
    def tearDownClass(cls):
        sys.path = cls.sys_path
        if os.path.exists(os.path.join(os.environ["HOME"], '.aws')):
            shutil.rmtree(os.path.join(os.environ["HOME"], '.aws'))

    def test_discover_auth_method_default_credentials_only(self):
        self.mocker.patch.dict(
            os.environ,
            {
                'AWS_ACCESS_KEY_ID': 'akiafkae',
                'AWS_SECRET_ACCESS_KEY': 'secretkey',
                'AWS_DEFAULT_REGION': 'test-region',
                'CLUSTER_NAME': 'test-cluster',
                'SERVICE_NAME': 'test-service',
                'AWS_OIDC_ROLE_ARN': ''
            }
        )

        ecs_pipe = ECSDeploy(schema=schema, check_for_newer_version=True)

        with self.caplog.at_level(logging.INFO):
            ecs_pipe.auth()

        self.assertIn("Using default authentication with AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY.", self.caplog.text)
        self.assertEqual(ecs_pipe.auth_method, 'DEFAULT_AUTH')
        self.assertFalse(os.path.exists(f'{os.environ["HOME"]}/.aws/.aws-oidc'))

    def test_discover_auth_method_oidc_only(self):
        self.mocker.patch.dict(
            os.environ,
            {
                'BITBUCKET_STEP_OIDC_TOKEN': 'token',
                'AWS_OIDC_ROLE_ARN': 'account/role',
                'AWS_DEFAULT_REGION': 'test-region',
                'CLUSTER_NAME': 'test-cluster',
                'SERVICE_NAME': 'test-service'
            }
        )
        ecs_pipe = ECSDeploy(schema=schema, check_for_newer_version=True)

        with self.caplog.at_level(logging.INFO):
            ecs_pipe.auth()

        self.assertIn("Authenticating with a OpenID Connect (OIDC) Web Identity Provider.", self.caplog.text)
        self.assertEqual(ecs_pipe.auth_method, 'OIDC_AUTH')
        self.assertTrue(os.path.exists(f'{os.environ["HOME"]}/.aws/.aws-oidc'))

        config = configparser.ConfigParser()
        config.read(f'{os.environ["HOME"]}/.aws/config')
        self.assertEqual(config['default'].get('role_arn'), 'account/role')
        token_file = config['default'].get('web_identity_token_file')

        with open(token_file) as f:
            self.assertEqual(f.read(), 'token')

    def test_discover_auth_method_oidc_and_default_credentials(self):
        self.mocker.patch.dict(
            os.environ,
            {
                'BITBUCKET_STEP_OIDC_TOKEN': 'token',
                'AWS_OIDC_ROLE_ARN': 'account/role',
                'AWS_ACCESS_KEY_ID': 'akiafkae',
                'AWS_SECRET_ACCESS_KEY': 'secretkey',
                'AWS_DEFAULT_REGION': 'test-region',
                'CLUSTER_NAME': 'test-cluster',
                'SERVICE_NAME': 'test-service'
            }
        )
        self.assertTrue('AWS_ACCESS_KEY_ID' in os.environ)
        self.assertTrue('AWS_SECRET_ACCESS_KEY' in os.environ)

        ecs_pipe = ECSDeploy(schema=schema, check_for_newer_version=True)

        with self.caplog.at_level(logging.INFO):
            ecs_pipe.auth()

        self.assertIn("Authenticating with a OpenID Connect (OIDC) Web Identity Provider.", self.caplog.text)
        self.assertEqual(ecs_pipe.auth_method, 'OIDC_AUTH')
        self.assertTrue(os.path.exists(f'{os.environ["HOME"]}/.aws/.aws-oidc'))
        config = configparser.ConfigParser()
        config.read(f'{os.environ["HOME"]}/.aws/config')
        self.assertEqual(config['default'].get('role_arn'), 'account/role')
        token_file = config['default'].get('web_identity_token_file')

        with open(token_file) as f:
            self.assertEqual(f.read(), 'token')

        self.assertFalse('AWS_ACCESS_KEY_ID' in os.environ)
        self.assertFalse('AWS_SECRET_ACCESS_KEY' in os.environ)
