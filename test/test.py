import os
import json

import pytest

from bitbucket_pipes_toolkit.test import PipeTestCase


CLUSTER_NAME = f"bbci-test-ecr-cluster-{os.getenv('BITBUCKET_BUILD_NUMBER')}"
SERVICE_NAME = f"bbci-test-ecr-service-{os.getenv('BITBUCKET_BUILD_NUMBER')}"


task_definition = """
{
   "containerDefinitions":[
      {
         "dnsSearchDomains":[

         ],
         "entryPoint":[

         ],
         "portMappings":[
            {
               "hostPort":0,
               "protocol":"tcp",
               "containerPort":80
            }
         ],
         "command":[

         ],
         "cpu":256,
         "environment":[

         ],
         "mountPoints":[

         ],

         "memoryReservation":512,
         "volumesFrom":[

         ],
         "essential":true,
         "links":[

         ],
         "name":"nginx",
         "image":"nginx"
      }
   ],
   "placementConstraints":[

   ],

   "family":"nginx",
   "requiresCompatibilities":[

   ],
   "volumes":[

   ]
}
"""


class ECSDeployTestCase(PipeTestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        # update task_definition
        td = json.loads(task_definition)
        td['family'] = SERVICE_NAME
        td['containerDefinitions'][0]['name'] = SERVICE_NAME

        with open('taskDefinition.json', 'w') as outfile:
            json.dump(td, outfile)

    @pytest.mark.order(1)
    def test_update_successful_with_wait_true(self):
        service_name = SERVICE_NAME
        result = self.run_container(environment={
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_DEFAULT_REGION': os.getenv('AWS_DEFAULT_REGION', 'us-east-1'),
            'CLUSTER_NAME': CLUSTER_NAME,
            'SERVICE_NAME': service_name,
            'TASK_DEFINITION': os.path.join(os.getcwd(), 'taskDefinition.json'),
            'WAIT': 'true'
        })

        self.assertRegex(
            result, rf'Service {service_name} has become stable')

    def test_update_successful(self):
        service_name = SERVICE_NAME
        result = self.run_container(environment={
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_DEFAULT_REGION': os.getenv('AWS_DEFAULT_REGION', 'us-east-1'),
            'CLUSTER_NAME': CLUSTER_NAME,
            'SERVICE_NAME': service_name,
            'TASK_DEFINITION': os.path.join(os.getcwd(), 'taskDefinition.json')
        })

        assert "Revision number" in result
        self.assertRegex(
            result, rf'✔ Successfully updated the {service_name} service')

    def test_oidc_update_successful(self):
        result = self.run_container(environment={
            'AWS_OIDC_ROLE_ARN': os.getenv('AWS_OIDC_ROLE_ARN'),
            'BITBUCKET_STEP_OIDC_TOKEN': os.getenv('BITBUCKET_STEP_OIDC_TOKEN'),
            'AWS_DEFAULT_REGION': os.getenv('AWS_DEFAULT_REGION', 'us-east-1'),
            'CLUSTER_NAME': CLUSTER_NAME,
            'SERVICE_NAME': SERVICE_NAME,
            'TASK_DEFINITION': os.path.join(os.getcwd(), 'taskDefinition.json')
        })
        self.assertRegex(
            result, rf'✔ Successfully updated the {SERVICE_NAME} service')

    def test_role_arn_success(self):
        result = self.run_container(environment={
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'AWS_DEFAULT_REGION': os.getenv('AWS_DEFAULT_REGION'),
            'AWS_ROLE_ARN': os.getenv('AWS_ROLE_ARN'),
            'AWS_ROLE_SESSION_NAME': os.getenv('AWS_ROLE_SESSION_NAME'),
            'CLUSTER_NAME': CLUSTER_NAME,
            'SERVICE_NAME': SERVICE_NAME,
            'TASK_DEFINITION': os.path.join(os.getcwd(), 'taskDefinition.json'),
            'DOCKER_HOST': 'tcp://host.docker.internal:2375'
        }, extra_hosts={"host.docker.internal": os.getenv('BITBUCKET_DOCKER_HOST_INTERNAL')})

        self.assertRegex(
            result, rf'✔ Successfully updated the {SERVICE_NAME} service')

    def test_update_successful_force_new_deployment(self):
        service_name = SERVICE_NAME
        result = self.run_container(environment={
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_DEFAULT_REGION': os.getenv('AWS_DEFAULT_REGION', 'us-east-1'),
            'CLUSTER_NAME': CLUSTER_NAME,
            'SERVICE_NAME': service_name,
            'FORCE_NEW_DEPLOYMENT': True
        })

        self.assertRegex(
            result, rf'✔ Successfully updated the {service_name} service')

    def test_update_successful_no_task_family_provided(self):
        service_name = SERVICE_NAME
        result = self.run_container(environment={
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_DEFAULT_REGION': os.getenv('AWS_DEFAULT_REGION', 'us-east-1'),
            'CLUSTER_NAME': CLUSTER_NAME,
            'SERVICE_NAME': service_name,
            'TASK_DEFINITION': os.path.join(os.getcwd(), 'taskDefinition.json'),
            'FORCE_NEW_DEPLOYMENT': True
        })

        self.assertRegex(
            result, rf'✔ Successfully updated the {service_name} service')

    def test_update_fails_if_cluster_doesnt_exist(self):
        service_name = SERVICE_NAME
        no_such_cluster = 'no-such-cluster'
        result = self.run_container(environment={
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_DEFAULT_REGION': os.getenv('AWS_DEFAULT_REGION', 'us-east-1'),
            'CLUSTER_NAME': no_such_cluster,
            'SERVICE_NAME': service_name,
            'TASK_DEFINITION': os.path.join(os.getcwd(), 'taskDefinition.json')
        })

        self.assertRegex(
            result, r'ECS cluster not found')

    def test_update_fails_if_service_doesnt_exist(self):
        service_name = 'no-such-service'
        result = self.run_container(environment={
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_DEFAULT_REGION': os.getenv('AWS_DEFAULT_REGION', 'us-east-1'),
            'CLUSTER_NAME': CLUSTER_NAME,
            'SERVICE_NAME': service_name,
            'TASK_DEFINITION': os.path.join(os.getcwd(), 'taskDefinition.json')
        })

        self.assertRegex(
            result, r'ECS service not found')

    def test_pipe_produces_extra_debug_info(self):
        result = self.run_container(environment={
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_DEFAULT_REGION': os.getenv('AWS_DEFAULT_REGION', 'us-east-1'),
            'CLUSTER_NAME': CLUSTER_NAME,
            'SERVICE_NAME': SERVICE_NAME,
            'TASK_DEFINITION': os.path.join(os.getcwd(), 'taskDefinition.json'),
            'FORCE_NEW_DEPLOYMENT': True,
            'DEBUG': 'true'
        }, stderr=True)

        self.assertRegex(
            result, r'DEBUG: Response body:')

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
        os.remove('taskDefinition.json')


class InvalidTaskDefinition(PipeTestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        with open('invalidTaskDefinition.json', 'w') as td:
            td.write('{')

    def test_update_fails_if_invalid_task_definition(self):
        service_name = 'no-such-service'
        result = self.run_container(environment={
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_DEFAULT_REGION': os.getenv('AWS_DEFAULT_REGION', 'us-east-1'),
            'CLUSTER_NAME': CLUSTER_NAME,
            'SERVICE_NAME': service_name,
            'TASK_FAMILY_NAME': os.getenv('ECS_TASK_FAMILY_NAME'),
            'TASK_DEFINITION': os.path.join(os.getcwd(), 'invalidTaskDefinition.json')
        })

        self.assertRegex(
            result, r'Failed to parse the task definition file')

    def test_update_fails_if_task_definition_file_does_not_exist(self):
        service_name = 'no-such-service'
        result = self.run_container(environment={
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_DEFAULT_REGION': os.getenv('AWS_DEFAULT_REGION', 'us-east-1'),
            'CLUSTER_NAME': CLUSTER_NAME,
            'SERVICE_NAME': service_name,
            'TASK_DEFINITION': os.path.join(os.getcwd(), 'nonExistentTaskDefinition.json')
        })

        self.assertRegex(
            result, r'Not able to find')

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
        os.remove('invalidTaskDefinition.json')


class InvalidParametersTaskDefinition(PipeTestCase):
    def setUp(self):
        super().setUpClass()
        with open('invalidTaskDefinitionParams.json', 'w') as td:
            td.write('{"wrong-param-name": "nginx:latest"}')

    def test_update_fails_if_invalid_task_definition(self):
        service_name = 'no-such-service'
        result = self.run_container(environment={
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_DEFAULT_REGION': os.getenv('AWS_DEFAULT_REGION', 'us-east-1'),
            'CLUSTER_NAME': CLUSTER_NAME,
            'SERVICE_NAME': service_name,
            'TASK_DEFINITION': os.path.join(os.getcwd(), 'invalidTaskDefinitionParams.json')
        })

        self.assertRegex(
            result, r'ECS task definition parameter validation error')

    def tearDown(self):
        super().tearDownClass()
        os.remove('invalidTaskDefinitionParams.json')
